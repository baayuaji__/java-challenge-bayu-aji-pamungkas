package ist.challenge.bayu_aji_pamungkas.DTO;

import lombok.Getter;

@Getter
public class UserRequestDTO {
    private String username;
    private String password;
}
