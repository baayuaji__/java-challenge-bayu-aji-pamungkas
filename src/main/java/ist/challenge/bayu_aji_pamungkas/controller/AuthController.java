package ist.challenge.bayu_aji_pamungkas.controller;

import ist.challenge.bayu_aji_pamungkas.DTO.UserRequestDTO;
import ist.challenge.bayu_aji_pamungkas.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class AuthController {

    @Autowired
    UserServiceImpl userServiceImpl;

    @PostMapping("/register")
    public ResponseEntity<Object> register(@RequestBody UserRequestDTO register) throws Exception {
        return userServiceImpl.registration(register);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody UserRequestDTO login) throws Exception {
        return userServiceImpl.login(login);
    }
}
