package ist.challenge.bayu_aji_pamungkas.controller;

import ist.challenge.bayu_aji_pamungkas.DTO.UserRequestDTO;
import ist.challenge.bayu_aji_pamungkas.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    UserServiceImpl userServiceImpl;

    @GetMapping("/")
    public ResponseEntity<Object> findAllUsers(Authentication authentication){
        return userServiceImpl.findAllUsers(authentication);
    }

    @PostMapping("/update")
    public ResponseEntity<Object> updateUser(@RequestBody UserRequestDTO updateUserDTO, Authentication authentication) throws Exception {
        return userServiceImpl.updateUser(updateUserDTO, authentication);
    }
}
