package ist.challenge.bayu_aji_pamungkas.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ResponseHandler {

    public static ResponseEntity<Object> generateResponse(String message, HttpStatus status, Integer statuscode, Object object){

        Map<String, Object> map = new HashMap<>();
        map.put("message", message);
        map.put("status", status);
        map.put("status code", statuscode);
        map.put("data", object);

        return new ResponseEntity<Object>(map, status);
    }
}
