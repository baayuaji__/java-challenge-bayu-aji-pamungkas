package ist.challenge.bayu_aji_pamungkas.repository;

import ist.challenge.bayu_aji_pamungkas.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByRoleName(String name);
}