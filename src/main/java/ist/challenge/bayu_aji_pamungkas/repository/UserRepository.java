package ist.challenge.bayu_aji_pamungkas.repository;

import ist.challenge.bayu_aji_pamungkas.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findByUserName(String name);
    boolean existsByUserName(String username);

}
