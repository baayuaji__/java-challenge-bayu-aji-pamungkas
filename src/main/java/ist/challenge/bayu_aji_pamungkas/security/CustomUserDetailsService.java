package ist.challenge.bayu_aji_pamungkas.security;

import ist.challenge.bayu_aji_pamungkas.entities.User;
import ist.challenge.bayu_aji_pamungkas.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("userDetailsService")
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    public CustomUserDetails loadUserByUsername(String userName) {

        User user = userRepository.findByUserName(userName).orElseThrow(() -> new UsernameNotFoundException("Username tidak ditemukan: " + userName));

        return CustomUserDetails.build(user);
    }
}
