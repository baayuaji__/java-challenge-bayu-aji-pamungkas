package ist.challenge.bayu_aji_pamungkas.service;

import ist.challenge.bayu_aji_pamungkas.DTO.UserRequestDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

public interface UserService {

    ResponseEntity<Object> login(UserRequestDTO userRequest)throws Exception;
    ResponseEntity<Object> findAllUsers(Authentication authentication) throws Exception;
    ResponseEntity<Object> registration(UserRequestDTO user)throws Exception;
    ResponseEntity<Object> updateUser(UserRequestDTO user, Authentication authentication)throws Exception;

}
