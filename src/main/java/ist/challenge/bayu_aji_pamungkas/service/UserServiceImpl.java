package ist.challenge.bayu_aji_pamungkas.service;

import ist.challenge.bayu_aji_pamungkas.DTO.UserRequestDTO;
import ist.challenge.bayu_aji_pamungkas.entities.Role;
import ist.challenge.bayu_aji_pamungkas.entities.User;
import ist.challenge.bayu_aji_pamungkas.handler.ResponseHandler;
import ist.challenge.bayu_aji_pamungkas.repository.RoleRepository;
import ist.challenge.bayu_aji_pamungkas.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    PasswordEncoder passwordEncoder;

    public ResponseEntity<Object> registration(UserRequestDTO registerDTO) throws Exception {

       try {
           boolean username = userRepository.existsByUserName(registerDTO.getUsername());
           if (username) {
               return ResponseHandler.generateResponse("Username sudah terpakai", HttpStatus.CONFLICT, 409, null);
           }

           //password format validator
           String password = registerDTO.getPassword();
           String passPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,20}$";
           if (!password.matches(passPattern)){
               throw new Exception("password min mengandung 8 karakter dengan minimal satu lowercase satu uppercase dan satu angka");
           }

           //new user
           User user = new User();
           user.setUserName(registerDTO.getUsername());

           // encrypt the password using spring security
           user.setPassword(passwordEncoder.encode(registerDTO.getPassword()));

           // register user default role
           Role role = roleRepository.findByRoleName("ROLE_USER");
           if(role == null){
               role = generateDefaultRole();
           }
           user.setRoles(List.of(role));

           User newUser = userRepository.save(user);

           return ResponseHandler.generateResponse("sukses registrasi! silahkan login", HttpStatus.CREATED, 201, newUser);
       } catch (Exception e){
           return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, 400,null);
       }

    }

    public ResponseEntity<Object> login(UserRequestDTO loginDTO) {

        try{

            if(loginDTO.getUsername().trim().isEmpty() || loginDTO.getPassword().trim().isEmpty()){
                return ResponseHandler.generateResponse("Username dan / atau password kosong", HttpStatus.BAD_REQUEST, 400, null);
            }

            //check for user
            Optional<User> userOptional = userRepository.findByUserName(loginDTO.getUsername());
            User user;

            if(userOptional.isPresent()){
                user = userOptional.get();
            } else {
                return ResponseHandler.generateResponse("User tidak ditemukan", HttpStatus.NOT_FOUND, 404, null);
            }

            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);

            return ResponseHandler.generateResponse("sukses login", HttpStatus.OK, 200, user);
        } catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, 400,null);
        }

    }

    public ResponseEntity<Object> findAllUsers(Authentication authentication) {

        try {
            //check credentials to access endpoint
            Optional<User> userOptional = userRepository.findByUserName(authentication.getName());
            if(userOptional.isEmpty()){
                return ResponseHandler.generateResponse("Bad Credentials: Silahkan login ulang dengan username dan password yang baru!", HttpStatus.UNAUTHORIZED, 401, null);
            }

            List<User> result = userRepository.findAll();

            return ResponseHandler.generateResponse("sukses menampilkan data user", HttpStatus.OK, 200, result);
        } catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR, 500, null);
        }
    }

    public ResponseEntity<Object> updateUser(UserRequestDTO userRequestDTO, Authentication authentication){

        try {

            //check credentials to access endpoint after updated username
            Optional<User> userOptional = userRepository.findByUserName(authentication.getName());
            User targetUser;
            if(userOptional.isPresent()){
                targetUser = userOptional.get();
            } else {
                return ResponseHandler.generateResponse("Silahkan login ulang setelah mengubah identitas username dan password baru!", HttpStatus.UNAUTHORIZED, 401, null);
            }

            //validate unique username
            Optional<User> checkUserName = userRepository.findByUserName(userRequestDTO.getUsername());
            if(checkUserName.isPresent()){
                return ResponseHandler.generateResponse("Username sudah terpakai", HttpStatus.CONFLICT, 409, null);
            } else {
                targetUser.setUserName(userRequestDTO.getUsername());
            }

            //validate different new password from old password
            if (passwordEncoder.matches(userRequestDTO.getPassword(),targetUser.getPassword())){
                return ResponseHandler.generateResponse("Password tidak boleh sama dengan password sebelumnya", HttpStatus.BAD_REQUEST, 400, null);
            } else {
                targetUser.setPassword(passwordEncoder.encode(userRequestDTO.getPassword()));
            }

            //database update
            User result = userRepository.save(targetUser);

            return ResponseHandler.generateResponse("sukses update informasi user!", HttpStatus.CREATED, 201, result);
        } catch (Exception e){
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, 400, null);
        }
    }

    private Role generateDefaultRole(){
        Role role = new Role();
        role.setRoleName("ROLE_USER");
        return roleRepository.save(role);
    }
}
